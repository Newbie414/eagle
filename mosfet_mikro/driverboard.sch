<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="no" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="moja2">
<packages>
<package name="SO-8">
<wire x1="-1.8034" y1="2.3622" x2="-1.8034" y2="-2.3622" width="0.127" layer="21"/>
<wire x1="-1.8034" y1="-2.3622" x2="1.8034" y2="-2.3622" width="0.127" layer="21"/>
<wire x1="1.8034" y1="-2.3622" x2="1.8034" y2="2.3622" width="0.127" layer="21"/>
<wire x1="1.8034" y1="2.3622" x2="-1.8034" y2="2.3622" width="0.127" layer="21"/>
<circle x="-1.2446" y="1.8034" radius="0.3556" width="0.0508" layer="21"/>
<text x="-2.032" y="-4.0005" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.032" y="2.7305" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.762" y="1.143" size="1.016" layer="21" ratio="10">8</text>
<rectangle x1="-2.8702" y1="1.7272" x2="-1.8542" y2="2.0828" layer="51"/>
<rectangle x1="-2.8702" y1="0.4572" x2="-1.8542" y2="0.8128" layer="51"/>
<rectangle x1="-2.8702" y1="-0.8128" x2="-1.8542" y2="-0.4572" layer="51"/>
<rectangle x1="-2.8702" y1="-2.0828" x2="-1.8542" y2="-1.7272" layer="51"/>
<rectangle x1="1.8542" y1="1.7272" x2="2.8702" y2="2.0828" layer="51"/>
<rectangle x1="1.8542" y1="0.4572" x2="2.8702" y2="0.8128" layer="51"/>
<rectangle x1="1.8542" y1="-0.8128" x2="2.8702" y2="-0.4572" layer="51"/>
<rectangle x1="1.8542" y1="-2.0828" x2="2.8702" y2="-1.7272" layer="51"/>
<smd name="P$1" x="-3.175" y="1.905" dx="2.286" dy="0.6096" layer="1"/>
<smd name="P$2" x="-3.175" y="0.635" dx="2.286" dy="0.6096" layer="1"/>
<smd name="P$3" x="-3.175" y="-0.635" dx="2.286" dy="0.6096" layer="1"/>
<smd name="P$4" x="-3.175" y="-1.905" dx="2.286" dy="0.6096" layer="1"/>
<smd name="P$5" x="3.175" y="1.905" dx="2.286" dy="0.6096" layer="1"/>
<smd name="P$6" x="3.175" y="0.635" dx="2.286" dy="0.6096" layer="1"/>
<smd name="P$7" x="3.175" y="-0.635" dx="2.286" dy="0.6096" layer="1"/>
<smd name="P$8" x="3.175" y="-1.905" dx="2.286" dy="0.6096" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="PN-FET">
<wire x1="-1.778" y1="4.572" x2="-1.778" y2="5.08" width="0.254" layer="94"/>
<wire x1="-1.778" y1="5.08" x2="-1.778" y2="5.588" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.794" x2="-1.778" y2="3.302" width="0.254" layer="94"/>
<wire x1="-1.778" y1="3.302" x2="-1.778" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.778" y1="5.08" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="7.112" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="6.604" x2="-1.778" y2="7.112" width="0.254" layer="94"/>
<wire x1="-1.778" y1="7.112" x2="-1.778" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.794" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.112" x2="-1.778" y2="7.112" width="0.1524" layer="94"/>
<wire x1="1.27" y1="3.302" x2="1.27" y2="5.588" width="0.1524" layer="94"/>
<wire x1="1.27" y1="5.588" x2="1.27" y2="7.112" width="0.1524" layer="94"/>
<wire x1="0" y1="7.112" x2="1.27" y2="7.112" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="3.302" x2="0" y2="3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="3.302" x2="1.27" y2="3.302" width="0.1524" layer="94"/>
<wire x1="0.508" y1="5.842" x2="0.762" y2="5.588" width="0.1524" layer="94"/>
<wire x1="0.762" y1="5.588" x2="1.27" y2="5.588" width="0.1524" layer="94"/>
<wire x1="1.27" y1="5.588" x2="1.778" y2="5.588" width="0.1524" layer="94"/>
<wire x1="1.778" y1="5.588" x2="2.032" y2="5.334" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-4.572" x2="-1.778" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-5.08" x2="-1.778" y2="-5.588" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.794" x2="-1.778" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-3.302" x2="-1.778" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-7.112" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-6.604" x2="-1.778" y2="-7.112" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-7.112" x2="-1.778" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.794" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.112" x2="-1.778" y2="-7.112" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-3.302" x2="1.27" y2="-4.572" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-4.572" x2="1.27" y2="-7.112" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.112" x2="1.27" y2="-7.112" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-3.302" x2="0" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.302" x2="1.27" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-4.318" x2="1.778" y2="-4.572" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-4.572" x2="1.27" y2="-4.572" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-4.572" x2="0.762" y2="-4.572" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-4.572" x2="0.508" y2="-4.826" width="0.1524" layer="94"/>
<wire x1="0" y1="3.302" x2="0" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="7.112" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.112" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<circle x="0" y="7.112" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="3.302" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-7.112" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-3.302" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="0" radius="0.3592" width="0" layer="94"/>
<text x="-11.43" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-11.43" y="0" size="1.778" layer="95">&gt;NAME</text>
<pin name="P-S" x="0" y="10.16" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="P-G" x="-5.08" y="7.62" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="N-S" x="0" y="-10.16" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="N-G" x="-5.08" y="-7.62" visible="off" length="short" direction="pas"/>
<polygon width="0.1524" layer="94">
<vertex x="1.27" y="5.588"/>
<vertex x="1.778" y="4.826"/>
<vertex x="0.762" y="4.826"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="0" y="5.08"/>
<vertex x="-1.016" y="5.588"/>
<vertex x="-1.016" y="4.572"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.27" y="-4.572"/>
<vertex x="0.762" y="-5.334"/>
<vertex x="1.778" y="-5.334"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-1.524" y="-5.08"/>
<vertex x="-0.508" y="-4.572"/>
<vertex x="-0.508" y="-5.588"/>
</polygon>
</symbol>
<symbol name="MC34151">
<wire x1="-7.62" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<pin name="NC." x="-12.7" y="7.62" length="middle" direction="nc"/>
<pin name="INA" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="GND" x="-12.7" y="-2.54" length="middle" direction="sup"/>
<pin name="INB" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="OUTB" x="12.7" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="VCC" x="12.7" y="-2.54" length="middle" direction="sup" rot="R180"/>
<pin name="OUTA" x="12.7" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="NC" x="12.7" y="7.62" length="middle" direction="nc" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SO8-PN-FET">
<gates>
<gate name="G$1" symbol="PN-FET" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO-8">
<connects>
<connect gate="G$1" pin="D" pad="P$5 P$6 P$7 P$8"/>
<connect gate="G$1" pin="N-G" pad="P$1"/>
<connect gate="G$1" pin="N-S" pad="P$2"/>
<connect gate="G$1" pin="P-G" pad="P$4"/>
<connect gate="G$1" pin="P-S" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MC34151">
<gates>
<gate name="G$1" symbol="MC34151" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO-8">
<connects>
<connect gate="G$1" pin="GND" pad="P$3"/>
<connect gate="G$1" pin="INA" pad="P$2"/>
<connect gate="G$1" pin="INB" pad="P$4"/>
<connect gate="G$1" pin="NC" pad="P$5"/>
<connect gate="G$1" pin="NC." pad="P$1"/>
<connect gate="G$1" pin="OUTA" pad="P$6"/>
<connect gate="G$1" pin="OUTB" pad="P$8"/>
<connect gate="G$1" pin="VCC" pad="P$7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diode">
<description>&lt;b&gt;Diodes&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Motorola : www.onsemi.com
&lt;li&gt;Fairchild : www.fairchildsemi.com
&lt;li&gt;Philips : www.semiconductors.com
&lt;li&gt;Vishay : www.vishay.de
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOD57-10">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
diameter 4 mm, vertical, grid 10.16 mm</description>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<wire x1="-1.143" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.635" x2="0.508" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-0.635" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0" x2="1.016" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0" x2="0.508" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-2.286" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="2.286" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21" curve="-131.11209"/>
<wire x1="-1.397" y1="-1.016" x2="1.397" y2="-1.016" width="0.1524" layer="21" curve="131.11209"/>
<wire x1="-2.286" y1="1.016" x2="-1.397" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-1.016" x2="2.286" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.635" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<pad name="C" x="-5.08" y="0" drill="1.1938" shape="long"/>
<pad name="A" x="5.08" y="0" drill="1.1938" shape="long"/>
<text x="-2.286" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.905" y1="-1.016" x2="-1.397" y2="1.016" layer="21"/>
<rectangle x1="-3.8354" y1="-0.4064" x2="-2.286" y2="0.4064" layer="21"/>
<rectangle x1="2.286" y1="-0.4064" x2="3.8354" y2="0.4064" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BYV27" prefix="D">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
ultra fast</description>
<gates>
<gate name="1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD57-10">
<connects>
<connect gate="1" pin="A" pad="A"/>
<connect gate="1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="moja2" deviceset="SO8-PN-FET" device=""/>
<part name="D2" library="diode" deviceset="BYV27" device=""/>
<part name="U$2" library="moja2" deviceset="SO8-PN-FET" device=""/>
<part name="D1" library="diode" deviceset="BYV27" device=""/>
<part name="D3" library="diode" deviceset="BYV27" device=""/>
<part name="D4" library="diode" deviceset="BYV27" device=""/>
<part name="U$3" library="moja2" deviceset="MC34151" device=""/>
<part name="U$4" library="moja2" deviceset="MC34151" device=""/>
<part name="P+1" library="supply1" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="20.32" y="71.12"/>
<instance part="D2" gate="1" x="24.13" y="76.2" rot="R90"/>
<instance part="U$2" gate="G$1" x="35.56" y="71.12" rot="MR0"/>
<instance part="D1" gate="1" x="31.75" y="76.2" rot="R90"/>
<instance part="D3" gate="1" x="24.13" y="66.04" rot="R90"/>
<instance part="D4" gate="1" x="31.75" y="66.04" rot="R90"/>
<instance part="U$3" gate="G$1" x="-12.7" y="71.12"/>
<instance part="U$4" gate="G$1" x="68.58" y="71.12" rot="MR0"/>
<instance part="P+1" gate="VCC" x="27.94" y="91.44"/>
<instance part="GND1" gate="1" x="27.94" y="49.53"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="D3" gate="1" pin="C"/>
<pinref part="D2" gate="1" pin="A"/>
<wire x1="24.13" y1="68.58" x2="24.13" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="D"/>
<wire x1="24.13" y1="71.12" x2="24.13" y2="73.66" width="0.1524" layer="91"/>
<wire x1="22.86" y1="71.12" x2="24.13" y2="71.12" width="0.1524" layer="91"/>
<junction x="24.13" y="71.12"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D1" gate="1" pin="A"/>
<pinref part="D4" gate="1" pin="C"/>
<wire x1="31.75" y1="73.66" x2="31.75" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="D"/>
<wire x1="31.75" y1="71.12" x2="31.75" y2="68.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="71.12" x2="31.75" y2="71.12" width="0.1524" layer="91"/>
<junction x="31.75" y="71.12"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="OUTA"/>
<wire x1="0" y1="73.66" x2="7.62" y2="73.66" width="0.1524" layer="91"/>
<wire x1="7.62" y1="73.66" x2="7.62" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P-G"/>
<wire x1="7.62" y1="78.74" x2="15.24" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="OUTB"/>
<pinref part="U$1" gate="G$1" pin="N-G"/>
<wire x1="0" y1="63.5" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="N-G"/>
<pinref part="U$4" gate="G$1" pin="OUTB"/>
<wire x1="40.64" y1="63.5" x2="55.88" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="OUTA"/>
<wire x1="55.88" y1="73.66" x2="48.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="48.26" y1="73.66" x2="48.26" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P-G"/>
<wire x1="48.26" y1="78.74" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="VCC"/>
<wire x1="0" y1="68.58" x2="13.97" y2="68.58" width="0.1524" layer="91"/>
<wire x1="13.97" y1="68.58" x2="13.97" y2="82.55" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="VCC"/>
<wire x1="55.88" y1="68.58" x2="43.18" y2="68.58" width="0.1524" layer="91"/>
<wire x1="43.18" y1="68.58" x2="43.18" y2="82.55" width="0.1524" layer="91"/>
<wire x1="24.13" y1="78.74" x2="24.13" y2="82.55" width="0.1524" layer="91"/>
<pinref part="D2" gate="1" pin="C"/>
<pinref part="U$1" gate="G$1" pin="P-S"/>
<wire x1="20.32" y1="81.28" x2="20.32" y2="82.55" width="0.1524" layer="91"/>
<wire x1="20.32" y1="82.55" x2="24.13" y2="82.55" width="0.1524" layer="91"/>
<pinref part="D1" gate="1" pin="C"/>
<wire x1="31.75" y1="78.74" x2="31.75" y2="82.55" width="0.1524" layer="91"/>
<wire x1="31.75" y1="82.55" x2="35.56" y2="82.55" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P-S"/>
<wire x1="35.56" y1="82.55" x2="35.56" y2="81.28" width="0.1524" layer="91"/>
<wire x1="24.13" y1="82.55" x2="27.94" y2="82.55" width="0.1524" layer="91"/>
<junction x="24.13" y="82.55"/>
<junction x="31.75" y="82.55"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="27.94" y1="82.55" x2="31.75" y2="82.55" width="0.1524" layer="91"/>
<wire x1="27.94" y1="88.9" x2="27.94" y2="82.55" width="0.1524" layer="91"/>
<junction x="27.94" y="82.55"/>
<wire x1="43.18" y1="82.55" x2="35.56" y2="82.55" width="0.1524" layer="91"/>
<junction x="35.56" y="82.55"/>
<wire x1="13.97" y1="82.55" x2="20.32" y2="82.55" width="0.1524" layer="91"/>
<junction x="20.32" y="82.55"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="GND"/>
<wire x1="81.28" y1="68.58" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
<wire x1="88.9" y1="68.58" x2="88.9" y2="54.61" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GND"/>
<wire x1="-25.4" y1="68.58" x2="-35.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="68.58" x2="-35.56" y2="54.61" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="N-S"/>
<wire x1="20.32" y1="60.96" x2="20.32" y2="58.42" width="0.1524" layer="91"/>
<pinref part="D3" gate="1" pin="A"/>
<wire x1="20.32" y1="58.42" x2="24.13" y2="58.42" width="0.1524" layer="91"/>
<wire x1="24.13" y1="58.42" x2="24.13" y2="63.5" width="0.1524" layer="91"/>
<pinref part="D4" gate="1" pin="A"/>
<wire x1="31.75" y1="63.5" x2="31.75" y2="58.42" width="0.1524" layer="91"/>
<wire x1="31.75" y1="58.42" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="N-S"/>
<wire x1="35.56" y1="58.42" x2="35.56" y2="60.96" width="0.1524" layer="91"/>
<wire x1="24.13" y1="58.42" x2="27.94" y2="58.42" width="0.1524" layer="91"/>
<junction x="24.13" y="58.42"/>
<junction x="31.75" y="58.42"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="27.94" y1="58.42" x2="31.75" y2="58.42" width="0.1524" layer="91"/>
<wire x1="27.94" y1="52.07" x2="27.94" y2="54.61" width="0.1524" layer="91"/>
<junction x="27.94" y="58.42"/>
<wire x1="27.94" y1="54.61" x2="27.94" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="54.61" x2="27.94" y2="54.61" width="0.1524" layer="91"/>
<junction x="27.94" y="54.61"/>
<wire x1="88.9" y1="54.61" x2="27.94" y2="54.61" width="0.1524" layer="91"/>
</segment>
</net>
<net name="INB" class="0">
<segment>
<wire x1="-38.1" y1="78.74" x2="-38.1" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="INB"/>
<wire x1="-38.1" y1="63.5" x2="-25.4" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="INA"/>
<wire x1="-25.4" y1="73.66" x2="-33.02" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="73.66" x2="-33.02" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="C" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="INA"/>
<wire x1="90.17" y1="81.28" x2="90.17" y2="73.66" width="0.1524" layer="91"/>
<wire x1="90.17" y1="73.66" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="INB"/>
<wire x1="95.25" y1="81.28" x2="95.25" y2="63.5" width="0.1524" layer="91"/>
<wire x1="95.25" y1="63.5" x2="81.28" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
